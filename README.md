## Flask REST API for Semantic shift detection


This repository contains a docker for semantic shift detection using the system proposed in paper [Scalable and Interpretable Semantic Change Detection](https://aclanthology.org/2021.naacl-main.369/)

### Requirements
-  docker
-  docker-compose


#### How to run it

The following command

```sh
$ docker-compose up -d --build
```

will build the images and run the containers. If you go to [http://localhost:5008](http://localhost:5008) you will see a web interface where you can check and test your REST API.

