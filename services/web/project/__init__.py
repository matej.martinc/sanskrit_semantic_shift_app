import os
import json

from flask import (
    Flask,
    jsonify,
    send_from_directory,
    request,
    redirect,
    url_for
)

from flask_restx import Api, Resource, fields, abort, reqparse
import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
from werkzeug.middleware.proxy_fix import ProxyFix

from . import semantic_shift_main as ss


app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.0',
          title='API services',
          description='Semantic shift detection REST API')
ns = api.namespace('rest_api', description='REST services API')

args = {
    'method': 'K5',
    'lang': 'sl',
    'corpus_slices': ['buddhist', 'nonbuddhist'],
    'max_df': 0.8,
    'topn': 15,
    'threshold_size_cluster': 5
}

if args['method'] == 'K7':
    method = 'kmeans_7'
elif args['method'] == 'K5':
    method = 'kmeans_5'
elif args['method'] == 'AP':
    method = 'aff_prop'


input_data = ss.loadData("project/data/" + method + "_labels.pkl",
                         "project/data/" + "sents.pkl",
                         "project/data/" + "id2sents.pkl")

word_list = ss.get_word_list("project/data/word_ranking.csv")

# input and output definitions
semantic_shift_input = api.model('SemanticShiftInput', {
    'word': fields.String(required=True, description='word you are interested in'),
})



@ns.route('/get_data/')
class Dataprovider(Resource):
    @ns.doc('Get back cluster distribution across time periods')
    @ns.expect(semantic_shift_input, validate=True)
    def post(self):
        distribution_data = ss.get_distribution_data(api.payload['word'], input_data, args)
        return {"distribution_data": distribution_data}

    def get(self):
        return word_list


@ns.route('/health/')
class Health(Resource):
    @ns.response(200, "successfully fetched health details")
    def get(self):
        return {"status": "running", "message": "Health check successful"}, 200, {}
