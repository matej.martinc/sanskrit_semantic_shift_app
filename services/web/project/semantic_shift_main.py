import dill
import numpy as np
import unidecode

from collections import Counter
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from collections import defaultdict



def loadData(labels_path, sentences_path, id2sents_path):
    labels = dill.load(open(labels_path, 'rb'))
    sentences = dill.load(open(sentences_path, 'rb'))
    id2sents = dill.load(open(id2sents_path, 'rb'))
    return labels, sentences, id2sents


def get_word_list(data):
    df = pd.read_csv(data, sep=';', encoding='utf8')
    return df.to_json(orient="records")


def get_stopwords():
    df = pd.read_csv('project/data/SktStopwordsJuly2023.csv', encoding='utf8', sep=',')
    sw = df['Var1'].tolist()
    sw = [w.strip() for w in sw]
    return set(sw)


def get_clusters_sent(target, threshold_size_cluster, labels, sentences, id2sents, corpus_slices):

    cluster_to_sentence = defaultdict(lambda: defaultdict(list))
    for cs in corpus_slices:
        for label, sents in zip(labels[target][cs], sentences[target][cs]):
            for sent in sents:
                sent_id = int(str(corpus_slices.index(cs) + 1) + str(sent))
                sent = id2sents[sent_id]
                cluster_to_sentence[label][cs].append(sent)

    counts = {cs: Counter(labels[target][cs]) for cs in corpus_slices}
    all_labels = []
    for slice, c in counts.items():
        slice_labels = [x[0] for x in c.items()]
        all_labels.extend(slice_labels)
    all_labels = set(all_labels)
    all_counts = []
    for l in all_labels:
        all_count = 0
        for slice in corpus_slices:
            count = counts[slice][l]
            all_count += count
        all_counts.append((l, all_count))

    sorted_counts = sorted(all_counts, key=lambda x: x[1], reverse=True)
    sentences = []
    lemmas = []
    metas = []
    labels = []
    categs = []

    for label, count in sorted_counts:
        #print("\n================================")
        #print("Cluster label: ", label, " - Cluster size: ", count)
        if count > threshold_size_cluster:
            for cs in corpus_slices:
                for (sent, lemma, meta) in cluster_to_sentence[label][cs]:
                    sent_clean = sent.strip()
                    lemma_clean = lemma.replace('_<ner>', '').strip()
                    time_token = '<' + str(cs) + '>'
                    sent_clean = sent_clean.replace(time_token, '').strip()
                    lemma_clean = lemma_clean.replace(time_token, '').strip()
                    if sent_clean not in set(sentences):
                        sentences.append(sent_clean)
                        lemmas.append(lemma_clean)
                        labels.append(label)
                        categs.append(cs)
                        metas.append(meta)
        else:
            print("Cluster", label, "is too small - deleted!")

    doc_ids = [x[0] for x in metas]
    years = [x for x in categs]
    sent_df = pd.DataFrame(list(zip(doc_ids, years, categs, labels, sentences, lemmas)),
                           columns=['doc_id', 'year_published', 'source', 'cluster_label', 'sentence',
                                    'lemmatized_sent'])
    return sent_df


def output_distrib(data, keyword_clusters):
    distrib = data.groupby(['source', "cluster_label"]).size().reset_index(name="count")
    pivot_distrib = distrib.pivot(index='source', columns='cluster_label', values='count')
    pivot_distrib_norm = pivot_distrib.div(pivot_distrib.sum(axis=1), axis=0)
    pivot_distrib_norm = pivot_distrib_norm.fillna(0)
    first_column = pivot_distrib_norm.columns[0]
    order = list(x for x in pivot_distrib_norm[first_column].keys())
    final_data = {}
    for i in keyword_clusters:
        name = "Cluster " + str(i) + ": " + ", ".join(keyword_clusters[i][:7])
        distrib = list(np.array(list(pivot_distrib_norm[i].fillna(0).array)))
        final_data[name] = distrib
    #final_data = sorted(final_data, reverse=True, key=lambda x: sum(x[1]))
    return final_data, order


def extract_topn_from_vector(feature_names, sorted_items, topn):
    """get the feature names and tf-idf score of top n items"""
    # use only topn items from vector
    sorted_items = sorted_items[:topn]

    score_vals = []
    feature_vals = []

    for idx, score in sorted_items:
        fname = feature_names[idx]
        # keep track of feature name and its corresponding score
        score_vals.append(round(score, 3))
        feature_vals.append(feature_names[idx])
    # create a tuples of feature,score
    # results = zip(feature_vals,score_vals)
    results = {}
    for idx in range(len(feature_vals)):
        results[feature_vals[idx]] = score_vals[idx]
    return results

def extract_keywords(target_word, word_clustered_data, max_df, topn):
    sw = get_stopwords()
    # get groups of sentences for each cluster
    l_sent_clust_dict = defaultdict(list)
    sent_clust_dict = defaultdict(list)
    for i, row in word_clustered_data.iterrows():
        sent = " ".join(row['sentence'].split())
        lemmatized_sent = " ".join(row['lemmatized_sent'].split())
        l_sent_clust_dict[row['cluster_label']].append((sent, lemmatized_sent))

    for label, data in l_sent_clust_dict.items():
        original_sents = "\t".join([x[0] for x in data])
        lemmas = "\t".join([x[1] for x in data])
        sent_clust_dict[label] = (original_sents, lemmas)

    labels = []
    lemmatized_clusters = []
    for label, (sents, lemmatized_sents) in sent_clust_dict.items():
        labels.append(label)
        lemmatized_clusters.append(lemmatized_sents)

    tfidf_transformer = TfidfVectorizer(smooth_idf=True, use_idf=True, ngram_range=(1,3), max_df=max_df, max_features=10000)
    tfidf_transformer.fit(lemmatized_clusters)
    feature_names = tfidf_transformer.get_feature_names()

    keyword_clusters = {}

    for label, lemmatized_cluster in zip(labels, lemmatized_clusters):
        # generate tf-idf
        tf_idf_vector = tfidf_transformer.transform([lemmatized_cluster])
        # sort the tf-idf vectors by descending order of scores
        tuples = zip(tf_idf_vector.tocoo().col, tf_idf_vector.tocoo().data)
        sorted_items = sorted(tuples, key=lambda x: (x[1], x[0]), reverse=True)
        # extract only the top n
        keywords = extract_topn_from_vector(feature_names, sorted_items, topn*10)
        keywords = sorted(keywords.items(), key=lambda x: x[1], reverse=True)
        scores = {x[0]:x[1] for x in keywords}

        already_in = set()
        filtered_keywords = []
        for kw, score in keywords:
            if len(kw.split()) == 1:
                for k, s in scores.items():
                    if kw in k and len(k.split()) > 1:
                        if score > s:
                            already_in.add(k)
                        else:
                            already_in.add(kw)
            if len(kw.split()) == 2:
                for k, s in scores.items():
                    if kw in k and len(k.split()) > 2:
                        if score > s:
                            already_in.add(k)
                        else:
                            already_in.add(kw)

            if kw not in already_in and kw != target_word:
                filtered_keywords.append(kw)
                already_in.add(kw)

        keyword_clusters[label] = filtered_keywords[:topn*10]

    final_keywords = {}
    all_data = []
    for c, keywords in keyword_clusters.items():
        sents = sent_clust_dict[c][0].split('\t')
        lemmas = sent_clust_dict[c][1].split('\t')
        all_sents = " ".join(sents)
        all_lemmas = " ".join(lemmas)
        set_lemmatized_sents = set(lemmas)
        filtered_keywords = []
        for kw in keywords:
            stop = 0
            for word in kw.split():
                if word in sw:
                    stop += 1
            if stop / float(len(kw.split())) < 0.3:
                 num_appearances = 0
                 for sent in set_lemmatized_sents:
                     if kw in sent:
                         num_appearances += 1
                 if num_appearances > 1:
                     if len(kw) > 2:
                         if kw + ' ' + target_word in all_lemmas:
                             kw = kw + ' ' + target_word
                         elif target_word + ' ' + kw in all_lemmas:
                             kw = target_word + ' ' + kw
                         filtered_keywords.append(kw)
        if len(filtered_keywords) == 0:
            filtered_keywords.append('other')
        final_keywords[c] = filtered_keywords[:topn]
        all_data.append((c, ";".join(filtered_keywords[:50]), all_sents))
    return final_keywords




def get_distribution_data(word, input_data, args):
    corpus_slices = args['corpus_slices']
    max_df = args['max_df']
    top_n = args['topn']
    threshold_size_cluster = args['threshold_size_cluster']

    labels, sentences, id2sent = input_data
    clusters_sents_df = get_clusters_sent(word, threshold_size_cluster, labels, sentences, id2sent, corpus_slices)
    keyword_clusters = extract_keywords(word, clusters_sents_df, topn=top_n, max_df=max_df)
    distrib, chunks = output_distrib(clusters_sents_df, keyword_clusters)
    return {"sent_info": clusters_sents_df.to_json(orient="records"),
            "keyword_info": keyword_clusters,
            "distrib_info": distrib,
            "chunks": chunks}

